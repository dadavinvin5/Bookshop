const searchEvent = (e) => {
    if (e.value) {
        table.innerHTML = ``

        bookList = JSON.parse(localStorage.getItem('listItem3')) ?? []
        bookList.filter(x => x.title.includes(e.value) || x.author.includes(e.value) || x.year.includes(e.value))
        .forEach((value, i) => {
            table.innerHTML += `
            <tr>
                <td>${i + 1}</td>
                <td>${value.title}</td>
                <td>${value.author}</td>
                <td>${value.year}</td>
                <td>
                    <button class="btn btn-sm btn-success" onclick="find(${value.id})">
                        <i class="fa fa-edit"></i>
                    </button>
                </td>
                <td>
                    <button class="btn btn-sm btn-danger" onclick="removeData4(${value.id})">
                        <i class="fa fa-trash"></i>
                    </button>
                </td>
            </tr>`
        });
        return;
    }
    allData();
};

function save() {
    bookList = JSON.parse(localStorage.getItem('listItem3')) ?? []
    var id
    bookList.length != 0 ? bookList.findLast((item) => id = item.id) : id = 0

    if (document.getElementById('inputBookId').value) {
        bookList.forEach(value => {
            if (document.getElementById('inputBookId').value == value.id) {
                value.title = document.getElementById('inputBookTitle').value,
                    value.author = document.getElementById('inputBookAuthor').value,
                    value.year = document.getElementById('inputBookYear').value,
                    value.isComplete = 1
            }
        });
        document.getElementById('inputBookId').value = ''
    } else {
        var item = {
            id: id + 1,
            title: document.getElementById('inputBookTitle').value,
            author: document.getElementById('inputBookAuthor').value,
            year: document.getElementById('inputBookYear').value,
            isComplete: 1,
        }
        bookList.push(item)
    }
    localStorage.setItem('listItem3', JSON.stringify(bookList))

    allData()
    document.getElementById('form').reset()
}

function allData() {

    table.innerHTML = ``
    bookList = JSON.parse(localStorage.getItem('listItem3')) ?? []
    bookList.forEach(function (value, i) {

        var table = document.getElementById('table')
        // if(value.isComplete == 0){
        table.innerHTML += `
            <tr>
                <td>${i + 1}</td>
                <td>${value.title}</td>
                <td>${value.author}</td>
                <td>${value.year}</td>
                <td>
                    <button class="btn btn-sm btn-success" onclick="find(${value.id})">
                        <i class="fa fa-edit"></i>
                    </button>
                </td>
                <td>
                    <button class="btn btn-sm btn-danger" onclick="removeData(${value.id})">
                        <i class="fa fa-trash"></i>
                    </button>
                </td>
            </tr>`
        // }
    })
}

function removeData(id) {
    bookList = JSON.parse(localStorage.getItem('listItem3')) ?? []
    bookList = bookList.filter(function (value) {
        return value.id != id;
    });
    localStorage.setItem('listItem3', JSON.stringify(bookList))
    allData()
}

function find(id) {
    bookList = JSON.parse(localStorage.getItem('listItem3')) ?? []
    bookList.forEach(function (value) {
        if (value.id == id) {
            document.getElementById('inputBookId').value = id
            document.getElementById('inputBookTitle').value = value.title
            document.getElementById('inputBookAuthor').value = value.author
            document.getElementById('inputBookYear').value = value.year
        }
    })
}

